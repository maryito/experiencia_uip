import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time

def initDatabase():
    cred = credentials.Certificate(".env/key.json")
    firebase_admin.initialize_app(cred,{'databaseURL': "https://experiecniauip.firebaseio.com/"})

def getData(id = None):
    ''' Service Get Data Firebase'''
    if id:
        data = '/uip/{}'.format(id)
    else:
        data = '/'
    ref = db.reference(data)
    return ref.get()

# prueba
# if __name__ == '__main__':
#     initDatabase()
#
#     while True:
#         # getData()
#         id = 'car1'
#         getData(id)
#         time.sleep(0.1)