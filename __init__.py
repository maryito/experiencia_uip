from service import initDatabase, getData
from config import CarNG
from time import sleep

if __name__ == '__main__':
    while True:
        try:
            nombre_carro = "car1"

            # Configurarmos el carro
            car = CarNG(nombre_carro)

            # Conexión de Base de datos - Firebase
            initDatabase()
            R = getData(id)

            print('---------------------------------------------')
            print('Server reply : ') + R

            if (R == '0'):
                print("Reposo")
                car.reposo()

            if (R == '1'):
                print("Adelante")
                car.adelante()

            if (R == '2'):
                print("Reversa")
                car.reversa()

            if (R == '3'):
                print("Derecha")
                car.derecha()

            if (R == '4'):
                print("Izquierda")
                car.izquierda()

        except:
            print("ERROR: no hay data en el db")
            # GPIO.cleanup()

        sleep(0.1)

