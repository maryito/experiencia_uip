#!usr/bin/env python
#-*-coding: utf-8 -*-

import RPi.GPIO as GPIO
from time import sleep
GPIO.setwarnings(False)

import sys

class CarNG:
    # Configuracion de los GPIO
    GPIO.setmode(GPIO.BOARD)

    # Asignacion de los GPIO    
    Motor1A = 16
    Motor1B = 18
    Motor1E = 22
    
    Motor2A = 23
    Motor2B = 21
    Motor2E = 19
    
    # Establecemos el tiempo en 0.1 defecto
    tiempo = 0.1
    nombre = ''
    
    def __init__(self, nombre, tiempo = None):
        print("Configurando el Carro - {}".format(nombre))
        if tiempo is not None:
            self.tiempo = tiempo
        if nombre is not None:
            self.nombre = nombre

        # Configuracion de pines
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.Motor1A, GPIO.OUT)
        GPIO.setup(self.Motor1B, GPIO.OUT)
        GPIO.setup(self.Motor1E, GPIO.OUT)

        GPIO.setup(self.Motor2A, GPIO.OUT)
        GPIO.setup(self.Motor2B, GPIO.OUT)
        GPIO.setup(self.Motor2E, GPIO.OUT)

        # self.adelante()
 
    def adelante(self):
        """ Acelerando CarNG"""
        print(self.adelante.__doc__)
        GPIO.output(self.Motor1A,GPIO.HIGH)
        GPIO.output(self.Motor1B,GPIO.LOW)
        GPIO.output(self.Motor1E,GPIO.HIGH)
        
        GPIO.output(self.Motor2A,GPIO.HIGH)
        GPIO.output(self.Motor2B,GPIO.LOW)
        GPIO.output(self.Motor2E,GPIO.HIGH)

        # sleep(self.tiempo)

    def reversa(self):
        """ Retrocediendo CarNG"""
        print(self.reversa.__doc__)
        GPIO.output(self.Motor1A,GPIO.LOW)
        GPIO.output(self.Motor1B,GPIO.HIGH)
        GPIO.output(self.Motor1E,GPIO.HIGH)
        
        GPIO.output(self.Motor2A,GPIO.HIGH)
        GPIO.output(self.Motor2B,GPIO.HIGH)
        GPIO.output(self.Motor2E,GPIO.LOW)
    
        # sleep(self.tiempo)

    #Motores derechos
    def derecha(self):
        """ Girando CarNG hacia la Derecha"""
        print(self.derecha.__doc__)
        GPIO.output(self.Motor1A,GPIO.LOW)
        GPIO.output(self.Motor1B,GPIO.LOW)
        GPIO.output(self.Motor1E,GPIO.LOW)
        
        GPIO.output(self.Motor2A,GPIO.HIGH)
        GPIO.output(self.Motor2B,GPIO.LOW)
        GPIO.output(self.Motor2E,GPIO.HIGH)
        
        # sleep(self.tiempo)

    #Motores Izquierdos
    def izquierda(self):
        """ Girando CarNG hacia la Izquierda"""
        print(self.izquierda.__doc__)
        GPIO.output(self.Motor1A,GPIO.HIGH)
        GPIO.output(self.Motor1B,GPIO.LOW)
        GPIO.output(self.Motor1E,GPIO.HIGH)
        
        GPIO.output(self.Motor2A,GPIO.LOW)
        GPIO.output(self.Motor2B,GPIO.LOW)
        GPIO.output(self.Motor2E,GPIO.LOW)
        
        # sleep(self.tiempo)

    def reposo(self):
        """ Apagando CarNG """
        print(self.reposo.__doc__, self.nombre)
        GPIO.output(self.Motor1E,GPIO.LOW)
        GPIO.output(self.Motor2E,GPIO.LOW)
        # GPIO.cleanup()
        # sleep(self.tiempo)

    def salir(self):
        """ Desconentando CarNG """
        print(self.salir.__doc__, self.nombre)
        GPIO.cleanup()
