



Sensor_IR = 7

#Configurar accion de los GPIO
GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor1E,GPIO.OUT)
 
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)
GPIO.setup(Motor2E,GPIO.OUT)

GPIO.setup(Sensor_IR,GPIO.IN)

#Definir canales de comunicacion
envi= socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

UDP_IP = "192.168.8.101"
UDP_PORT = 61559
UDP_PORTR = 61557

UDP_IP_2 = "192.168.8.103"
UDP_PORT_2 = 61547
UDP_PORTR_2 = 61549
 
i=0

while True:

#Codigo de envio
      envi = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
#Codigo de recibido de datos
      try:
    
            #reci=socket(AF_INET, SOCK_DGRAM) #escuchar vehiculo
            reci = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            reci.settimeout(5)
            reci.bind(('192.168.8.104', UDP_PORTR))
            d=reci.recvfrom(1023)
            b=reci.recvfrom(1023)
            r=reci.recvfrom(1023)
            reci.close()
        
        
            X = d[0]
            Y = b[0]
            R = r[0]    

            print ('---------------------------------------------')
            print ('Server reply : ') + R
            
            if (R == '0'):
                  print ("Reposo")
                  GPIO.output(Motor1E,GPIO.LOW)
                  GPIO.output(Motor2E,GPIO.LOW)
                  #envio de healthbeat  
                  envi.sendto(str(R),(UDP_IP_2,UDP_PORT_2))
                  
            if (R == '1'):
                  print ("Adelante")
                  GPIO.output(Motor1A,GPIO.HIGH)
                  GPIO.output(Motor1B,GPIO.LOW)
                  GPIO.output(Motor1E,GPIO.HIGH)
                   
                  GPIO.output(Motor2A,GPIO.HIGH)
                  GPIO.output(Motor2B,GPIO.LOW)
                  GPIO.output(Motor2E,GPIO.HIGH)
                  envi.sendto(str(R),(UDP_IP_2,UDP_PORT_2))
                  
            if (R == '2'):
                  print ("Reversa")
                  GPIO.output(Motor1A,GPIO.HIGH)
                  GPIO.output(Motor1B,GPIO.HIGH)
                  GPIO.output(Motor1E,GPIO.LOW)
                   
                  GPIO.output(Motor2A,GPIO.LOW)
                  GPIO.output(Motor2B,GPIO.HIGH)
                  GPIO.output(Motor2E,GPIO.HIGH)
                  envi.sendto(str(R),(UDP_IP_2,UDP_PORT_2))
                  
            if (R == '3'):
                  print ("Derecha")
                  GPIO.output(Motor1A,GPIO.HIGH)
                  GPIO.output(Motor1B,GPIO.LOW)
                  GPIO.output(Motor1E,GPIO.HIGH)

                  GPIO.output(Motor2A,GPIO.LOW)
                  GPIO.output(Motor2B,GPIO.HIGH)
                  GPIO.output(Motor2E,GPIO.LOW)
                  envi.sendto(str(R),(UDP_IP_2,UDP_PORT_2))
                  
            if (R == '4'):
                  print ("Izquierda")
                  GPIO.output(Motor1A,GPIO.LOW)
                  GPIO.output(Motor1B,GPIO.HIGH)
                  GPIO.output(Motor1E,GPIO.LOW)
                  
                  GPIO.output(Motor2A,GPIO.HIGH)
                  GPIO.output(Motor2B,GPIO.LOW)
                  GPIO.output(Motor2E,GPIO.HIGH)
                  envi.sendto(str(R),(UDP_IP_2,UDP_PORT_2))
            
            if GPIO.input(Sensor_IR) == True:
                  print "Sin obstaculos"
                  
            else:
                  print "Obstaculo detectado"
                  GPIO.output(Motor1E,GPIO.LOW)
                  GPIO.output(Motor2E,GPIO.LOW)
                  envi.sendto(str(5),(UDP_IP_2,UDP_PORT_2))
        
      except:
            print ("ERROR: no hay data en el socket")
            #GPIO.cleanup()
            

      time.sleep(0.1)
    
      i+=1

    
