#!usr/bin/env python
# -*-coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import socket
import sys
from service import *

# Configuracion de los GPIO
GPIO.setmode(GPIO.BOARD)

# Asignacion de los GPIO
Motor1A = 16
Motor1B = 18
Motor1E = 22

Motor2A = 23
Motor2B = 21
Motor2E = 19

Sensor_IR = 7

# Configurar accion de los GPIO
GPIO.setup(Motor1A, GPIO.OUT)
GPIO.setup(Motor1B, GPIO.OUT)
GPIO.setup(Motor1E, GPIO.OUT)

GPIO.setup(Motor2A, GPIO.OUT)
GPIO.setup(Motor2B, GPIO.OUT)
GPIO.setup(Motor2E, GPIO.OUT)

GPIO.setup(Sensor_IR, GPIO.IN)

# Conexión de Base de datos - Firebase
initDatabase()

UDP_IP = "192.168.8.101"
UDP_PORT = 61559
UDP_PORTR = 61557

UDP_IP_2 = "192.168.8.103"
UDP_PORT_2 = 61547
UDP_PORTR_2 = 61549


while True:

    # Codigo de envio
    envi = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Codigo de recibido de datos
    try:
        id = "car1"
        R = getData(id)

        print('---------------------------------------------')
        print('Server reply : ') + R

        if (R == '0'):
            print("Reposo")
            GPIO.output(Motor1E, GPIO.LOW)
            GPIO.output(Motor2E, GPIO.LOW)

        if (R == '1'):
            print("Adelante")
            GPIO.output(Motor1A, GPIO.HIGH)
            GPIO.output(Motor1B, GPIO.LOW)
            GPIO.output(Motor1E, GPIO.HIGH)

            GPIO.output(Motor2A, GPIO.HIGH)
            GPIO.output(Motor2B, GPIO.LOW)
            GPIO.output(Motor2E, GPIO.HIGH)

        if (R == '2'):
            print("Reversa")
            GPIO.output(Motor1A, GPIO.HIGH)
            GPIO.output(Motor1B, GPIO.HIGH)
            GPIO.output(Motor1E, GPIO.LOW)

            GPIO.output(Motor2A, GPIO.LOW)
            GPIO.output(Motor2B, GPIO.HIGH)
            GPIO.output(Motor2E, GPIO.HIGH)

        if (R == '3'):
            print("Derecha")
            GPIO.output(Motor1A, GPIO.HIGH)
            GPIO.output(Motor1B, GPIO.LOW)
            GPIO.output(Motor1E, GPIO.HIGH)

            GPIO.output(Motor2A, GPIO.LOW)
            GPIO.output(Motor2B, GPIO.HIGH)
            GPIO.output(Motor2E, GPIO.LOW)

        if (R == '4'):
            print("Izquierda")
            GPIO.output(Motor1A, GPIO.LOW)
            GPIO.output(Motor1B, GPIO.HIGH)
            GPIO.output(Motor1E, GPIO.LOW)

            GPIO.output(Motor2A, GPIO.HIGH)
            GPIO.output(Motor2B, GPIO.LOW)
            GPIO.output(Motor2E, GPIO.HIGH)

    except:
        print("ERROR: no hay data en el db")
        # GPIO.cleanup()

    time.sleep(0.1)


